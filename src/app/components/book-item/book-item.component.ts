import { Component, OnInit,Input } from '@angular/core';

@Component({
  selector: 'app-book-item',
  templateUrl: './book-item.component.html',
  styleUrls: ['./book-item.component.scss']
})
export class BookItemComponent implements OnInit {
  @Input() book;
  constructor() {
    console.log(this.book)
   }

  ngOnInit(): void {
  }

}
