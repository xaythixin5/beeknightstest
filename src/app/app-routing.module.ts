import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeModule } from "./home/home.module";

const routes: Routes = [
  //Trang HOME - localhost:4200
  {
    path: "",
    loadChildren: () => HomeModule
  },
  //Trang Admin - localhost:4200/admin
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
