import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./home.component";
import { NavbarComponent } from "../components/navbar/navbar.component";
import { BookItemComponent } from '../components/book-item/book-item.component';

@NgModule({
  declarations: [HomeComponent, NavbarComponent,BookItemComponent],
  imports: [CommonModule, HomeRoutingModule]
})
export class HomeModule {}
