import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/_core/service/data.service';
import { Subscription } from 'rxjs';
// import ResizeService  from 'module'
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  listBook: any = [];
  subListBook: Subscription;
  
  constructor(private dataService: DataService) { }

  getListBook() {
    const uri = 'books?pageSize=30'
    this.subListBook = this.dataService.getListBook(uri).subscribe((result: any) => {
      console.log(result);
      this.listBook = result;
    });
  }
  click() {
    console.log("object")
  }
  ngOnchanges() {
    console.log("ngOnchanges");
  }

  ngOnInit(): void {
    this.getListBook();
  }

  ngAfterViewInit() {
    //Chờ HTML render xong hết, nó sẽ chạy. Để DOM
    console.log("ngAfterViewInit");
  }

  ngOnDestroy() {
    this.subListBook.unsubscribe;
  }
}
