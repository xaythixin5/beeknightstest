import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { tap, catchError } from "rxjs/operators";
import { environment } from "./../../../environments/environment";

let urlApi = "";

@Injectable({
  providedIn: "root"
})
export class DataService {
  constructor(private http: HttpClient) {
    urlApi = environment.urlApi;
  }

  getListBook(uri): Observable<any> {
    return this.http.get(urlApi + "/" + uri).pipe(
      tap(() => {}),
      catchError(error => {
        return this.handleError(error);
      })
    );
  }

  handleError(error) {
    //Xử lý lỗi
    switch (error.status) {
      //Vì dụ status là mã lỗi: 401, 300, 402, 403...
      case 401:
        break;
      case 403:
        break;
      case 500:
        alert(error.error);
        break;

      default:
        break;
    }
    throwError(error);
    return error;
  }
}
