Cảm ơn quý công ty đã cho em cơ hội thực hiện bài test coding.
Bài test được thực hiện lúc 11h00 25/06/2020. Tổng thời gian thực hiện khoảng 5 tiếng.
Bao Gồm:    - ~30p phân tích thiết kế,
            - ~60p tìm kiếm công cụ và bổ sung kĩ năng
            - ~3.5h thực hiện code, dựng và chỉnh sửa.
Em nhận được yêu cầu bao gồm:   - Dàn layout theo template cho san bằng file PSD.
                                - Thực hiện fetch data từ server và hiển thị data theo mẫu.
                                - Bài test được dựng bằng angular framework version 9.1.5
Đã đạt được:    - thực hiện 2 yêu cầu trên tại width > 1280.
                - thực hiện fetch data tu server
Chưa đạt được:  - Chưa Reponsive app tại các size màn hình nhỏ hơn (chưa làm kịp).
                - Chưa chau chuốt UI và UX.
Tự nhận xét cơ bản hoàn thành phần khung của bài test, hoàn thành khoảng 60% yêu cầu đề ra. Nhưng còn rất nhiều thiết sót.

# beeknightstest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
